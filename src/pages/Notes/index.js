import React, { Component } from 'react'
import Header from '../../components/Header';

import { listNotes, saveNotes, excluirNotes } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    text: '',
    editando: false,
    excluindo: false
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true,
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: '',
      editando: false,
      excluindo: false
    }) 
  }
  
  editarNote = async () => {
    this.setState({
      editando: true
    })
  }

  delNote = async () => {
    this.setState({
      editando: true,
      excluindo: true
    })
    await excluirNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: '',
      editando: false
    }) 
  }

  render() {
    return (
      <div>
        <Header/>
        <div className='content'>          
          <div className='filters'>
            <div className='left'>
              <input type='search' placeholder='Procurar...' />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
            { this.state.editando && (
              this.state.notes.map(note => (
                <div className='item' key={note.id}>
                  <input onChange={this.editText} className='left' type='text' defaultValue={note.text} placeholder='Digite sua anotação' />
                  <div className='right'>
                    { this.state.editando && !this.state.loadingSave && !this.state.excluindo && (<button onClick={this.saveNote}>Salvar</button>) }
                    { this.state.loadingSave && (<span>Salvando...</span>) }
                    { this.state.excluindo && (<span>Excluindo...</span>)}
                  </div>
                </div>
              ))
            )}
            { !this.state.editando && (this.state.notes.map(note => (
              <div className='item' key={note.id}>
                <input onChange={this.editText} className='left' type='text' defaultValue={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { !note.id && !this.state.editando && (<button onClick={this.saveNote}>Salvar</button>) }
                  { note.id && !this.state.editando && (<button onClick={this.delNote}>Excluir</button>) }
                  { note.id && !this.state.editando && (<button onClick={this.editarNote}>Editar</button>) }
                </div>
              </div>
            ))
            )}
          </div>
        </div>
      </div>
    )
  }
}
