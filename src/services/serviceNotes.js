const result = [
    { text: 'Lavar o carro', id: Math.random() * 2},
    { text: 'Dar vacina na Alice', id: Math.random() * 2 }
]

const listNotes = () => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        }, 2000)
    })
}


const saveNotes = (text) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.push({
                text: text,
                id: new Date().getTime()
            })
            resolve()
        }, 2000)
    })
}


const excluirNotes = (text) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.pop({
                text: text,
                id: Math.random() * 2
            })
            resolve()
        }, 2000)
    })
}

const updateNotes = (id, text) => {
    // return new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         for (var x=0; x<=result.length-1; x++){
    //             if (id === result[x].id){
    //                 result[x].text = text;
    //             }
    //         }
    //     })
    //     resolve()
    // }, 2000)
}


module.exports = {
    listNotes,
    saveNotes,
    excluirNotes
}